package ch.gotdns.kikuchi.applicationno1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SeterAlarmu extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, UslugaAlarmu.class);
        service.setAction(UslugaAlarmu.CREATE);
        context.startService(service);
    }
}
