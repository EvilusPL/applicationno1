package ch.gotdns.kikuchi.applicationno1;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UsunKontakt extends AppCompatActivity {

    private ListView lstWvDeleteContact;
    private ArrayList<Kontakt> kontakty;
    private ArrayAdapter<Kontakt> arrayAdapter;

    private ArrayList<Kontakt> odczytajKontakty(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("kontakty", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Kontakt>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<Kontakt>();
    }

    private void zapiszKontakty(ArrayList<Kontakt> notatki, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("kontakty", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notatki);
        editor.putString(key, json);
        editor.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usun_kontakt);

        kontakty = odczytajKontakty("kontakty");
        lstWvDeleteContact = findViewById(R.id.lstWvDeleteContact);

        lstWvDeleteContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kontakty.remove(position);
                zapiszKontakty(kontakty, "kontakty");
                Toast.makeText(UsunKontakt.this, "Usunięto kontakt!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        arrayAdapter = new ArrayAdapter<Kontakt>(this, R.layout.row_ksiazkatelefoniczna, R.id.nameTextView, kontakty) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                Bitmap bitmap;
                TextView text1 = view.findViewById(R.id.nameTextView);
                TextView text2 = view.findViewById(R.id.numberTextView);
                ImageView imageView = view.findViewById(R.id.avatarImageView);
                text1.setText(kontakty.get(position).getImie() + " " + kontakty.get(position).getNazwisko());
                text2.setText(kontakty.get(position).getNrTelefonu());
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(kontakty.get(position).getAwatar(), 0, kontakty.get(position).getAwatar().length));
                return view;
            }
        };

        lstWvDeleteContact.setAdapter(arrayAdapter);
    }
}
