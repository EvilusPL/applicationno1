package ch.gotdns.kikuchi.applicationno1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PodgladNotatki extends AppCompatActivity {

    public static String tytul;
    public static String tresc;
    public static String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podglad_notatki);

        TextView txtWvTytulNotatki = findViewById(R.id.txtVwTytulNotatki);
        TextView txtWvTrescNotatki = findViewById(R.id.txtWvTrescNotatki);
        TextView txtWvData = findViewById(R.id.txtWvData);

        txtWvTytulNotatki.setText(tytul);
        txtWvTrescNotatki.setText(tresc);
        txtWvData.setText(data);
    }
}
