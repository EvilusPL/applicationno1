package ch.gotdns.kikuchi.applicationno1;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UsuwanieNotatki extends AppCompatActivity {

    private ArrayList<FormatNotatki> notatki;
    private ArrayAdapter<FormatNotatki> arrayAdapter;

    private void zapiszNotatke(ArrayList<FormatNotatki> notatki, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("notatki", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notatki);
        editor.putString(key, json);
        editor.apply();
    }

    private ArrayList<FormatNotatki> odczytajNotatki(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("notatki", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatNotatki>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatNotatki>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuwanie_notatki);

        notatki = odczytajNotatki("notatki");
        ListView lstWvDoUsuniecia = findViewById(R.id.lstWvDoUsuniecia);

        arrayAdapter = new ArrayAdapter<FormatNotatki>(this, android.R.layout.simple_list_item_2, android.R.id.text1, notatki) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                TextView text1 = view.findViewById(android.R.id.text1);
                TextView text2 = view.findViewById(android.R.id.text2);
                text1.setText(notatki.get(position).getTytul());
                text2.setText(notatki.get(position).getDate());
                return view;
            }
        };

        lstWvDoUsuniecia.setAdapter(arrayAdapter);

        lstWvDoUsuniecia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                notatki.remove(position);
                zapiszNotatke(notatki, "notatki");
                Toast.makeText(UsuwanieNotatki.this, "Usunięto notatkę!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
