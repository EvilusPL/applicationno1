package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class KsiazkaTelefoniczna extends AppCompatActivity {

    private ListView lstWvPhoneBook;
    private FloatingActionButton btnAddContact;
    private FloatingActionButton btnDeleteContact;
    private EditText edTxtSearchContact;
    private ArrayList<Kontakt> kontakty;
    private ArrayList<Kontakt> wszystkieKontakty;
    private ArrayAdapter<Kontakt> arrayAdapter;

    private ArrayList<Kontakt> odczytajKontakty(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("kontakty", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Kontakt>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<Kontakt>();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ksiazka_telefoniczna);

        btnAddContact = findViewById(R.id.btnAddNumber);
        btnDeleteContact = findViewById(R.id.btnDeleteNumber);
        edTxtSearchContact = findViewById(R.id.edTxtSearchContact);
        lstWvPhoneBook = findViewById(R.id.lstWvPhoneBook);

        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KsiazkaTelefoniczna.this, DodajKontakt.class);
                startActivity(intent);
            }
        });

        btnDeleteContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KsiazkaTelefoniczna.this, UsunKontakt.class);
                startActivity(intent);
            }
        });

        lstWvPhoneBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + kontakty.get(position).getNrTelefonu()));
                startActivity(intent);
            }
        });

        edTxtSearchContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                arrayAdapter.getFilter().filter(edTxtSearchContact.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        kontakty = odczytajKontakty("kontakty");
        wszystkieKontakty = odczytajKontakty("kontakty");

        arrayAdapter = new ArrayAdapter<Kontakt>(this, R.layout.row_ksiazkatelefoniczna, R.id.nameTextView, kontakty) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                Bitmap bitmap;
                TextView text1 = view.findViewById(R.id.nameTextView);
                TextView text2 = view.findViewById(R.id.numberTextView);
                ImageView imageView = view.findViewById(R.id.avatarImageView);
                text1.setText(kontakty.get(position).getImie()+" "+kontakty.get(position).getNazwisko());
                text2.setText(kontakty.get(position).getNrTelefonu());
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(kontakty.get(position).getAwatar(), 0, kontakty.get(position).getAwatar().length));
                return view;
            }

            @Override
            public Filter getFilter() {
                Filter filter = new Filter() {
                    @Override
                    protected void publishResults(CharSequence constraint, FilterResults results) {
                        kontakty = (ArrayList<Kontakt>) results.values;
                        if (kontakty.size() == 0) {
                            Toast.makeText(KsiazkaTelefoniczna.this, "Nic nie znaleziono! :(", Toast.LENGTH_SHORT).show();
                        }
                        arrayAdapter.clear();
                        arrayAdapter.addAll(kontakty);
                        notifyDataSetChanged();
                    }

                    @Override
                    protected FilterResults performFiltering(CharSequence constraint) {
                        FilterResults filterResults = new FilterResults();
                        ArrayList<Kontakt> resultsArray = new ArrayList<Kontakt>();

                        if (constraint == null || constraint.length() == 0) {
                            filterResults.values = wszystkieKontakty;
                            filterResults.count = wszystkieKontakty.size();
                        } else {
                            constraint = constraint.toString().toLowerCase();
                            for (int i=0; i<wszystkieKontakty.size(); i++) {
                                String tytulKontaktu = wszystkieKontakty.get(i).getImie()+" "+wszystkieKontakty.get(i).getNazwisko();
                                if (tytulKontaktu.toLowerCase().contains(constraint)) {
                                    resultsArray.add(wszystkieKontakty.get(i));
                                }
                            }
                            filterResults.count = resultsArray.size();
                            filterResults.values = resultsArray;
                        }

                        return filterResults;
                    }
                };
                return filter;
            }
        };

        lstWvPhoneBook.setAdapter(arrayAdapter);
    }
}
