package ch.gotdns.kikuchi.applicationno1;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

public class UslugaAlarmu extends IntentService {
    public static final String CREATE = "CREATE";
    public static final String CANCEL = "CANCEL";
    public static final String REMOVE = "REMOVE";

    private IntentFilter intentFilter;

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    private void zapiszPrzypomnienie(ArrayList<FormatPrzypomnienia> przypomnienia, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(przypomnienia);
        editor.putString(key, json);
        editor.apply();
    }

    public UslugaAlarmu() {
        super("UslugaAlarmu");
        intentFilter = new IntentFilter();
        intentFilter.addAction(CREATE);
        intentFilter.addAction(CANCEL);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        int reminderId = intent.getIntExtra("notificationId", 0);

        if (intentFilter.matchAction(action))  {
            execute(action, reminderId);
        }
    }

    private void execute(String action, int notificationId) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        ArrayList<FormatPrzypomnienia> przypomnienia = odczytajPrzypomnienia("przypomnienia");
        FormatPrzypomnienia przypomnienie = przypomnienia.get(notificationId);

        Intent intent = new Intent(this, OdbiornikPrzypomnienia.class);
        intent.putExtra("reminderId", notificationId);
        intent.putExtra("description",  przypomnienie.getDescription());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar alarmCalendar = Calendar.getInstance();
        alarmCalendar.setTime(przypomnienie.getAlarmDate());
        long time = alarmCalendar.getTimeInMillis();

        if (CREATE.equals(action)) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        } else if (CANCEL.equals(action)) {
            alarmManager.cancel(pendingIntent);
        } else if (REMOVE.equals(action)) {
            alarmManager.cancel(pendingIntent);
            przypomnienia.remove(przypomnienie);
            zapiszPrzypomnienie(przypomnienia, "przypomnienia");
        }
    }
}
