package ch.gotdns.kikuchi.applicationno1;

import android.graphics.Bitmap;

public class Kontakt {
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNrTelefonu() {
        return nrTelefonu;
    }

    public void setNrTelefonu(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }

    public String getAdresEmail() {
        return adresEmail;
    }

    public void setAdresEmail(String adresEmail) {
        this.adresEmail = adresEmail;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public byte[] getAwatar() {
        return awatar;
    }

    public void setAwatar(byte[] awatar) {
        this.awatar = awatar;
    }

    public Kontakt(String imie, String nazwisko, String nrTelefonu, String adresEmail, String plec, byte[] awatar) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrTelefonu = nrTelefonu;
        this.adresEmail = adresEmail;
        this.plec = plec;
        this.awatar = awatar;
    }

    private String imie;
    private String nazwisko;
    private String nrTelefonu;
    private String adresEmail;
    private String plec;
    private byte[] awatar;


}
