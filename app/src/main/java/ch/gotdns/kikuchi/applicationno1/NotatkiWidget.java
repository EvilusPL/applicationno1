package ch.gotdns.kikuchi.applicationno1;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class NotatkiWidget extends AppWidgetProvider {

    private ArrayList<FormatNotatki> odczytajNotatki(String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("notatki", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatNotatki>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatNotatki>();
    }

    private Integer odczytajPozycje(String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ostatnia_notatka", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<Integer>() {}.getType();
        return gson.fromJson(json, type);
    }

    private void zapiszPozycje(Integer pozycja, String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ostatnia_notatka", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(pozycja);
        editor.putString(key, json);
        editor.apply();
    }

    private void czyscPozycje(String key, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ostatnia_notatka", 0);
        sharedPreferences.edit().remove(key).apply();
    }

    private void widgetRoutine(RemoteViews views, Context context, String appWidgetIdString, AppWidgetManager appWidgetManager, Intent intent, int appWidgetId) {

        ArrayList<FormatNotatki> notatki = odczytajNotatki("notatki", context);
        FormatNotatki notatka;
        int pozycja;
        CharSequence tytul, tresc;

        if (odczytajPozycje(appWidgetIdString, context) != null) {
            pozycja = odczytajPozycje(appWidgetIdString, context);
        } else {
            pozycja = 0;
            zapiszPozycje(pozycja, appWidgetIdString, context);
        }

        if (notatki.size() == 0) {
            views.setTextViewText(R.id.txtWvTytulNotatkiWidget, "Nie ma notatek");
            views.setTextViewText(R.id.txtWvTrescNotatkiWidget, "Proszę, dodaj jakąś!");
        } else {
            notatka = notatki.get(pozycja);
            tytul = notatka.getTytul();
            tresc = notatka.getTresc();

            views.setTextViewText(R.id.txtWvTytulNotatkiWidget, tytul);
            views.setTextViewText(R.id.txtWvTrescNotatkiWidget, tresc);
            appWidgetManager.updateAppWidget(appWidgetId, views);

            if (pozycja < notatki.size() - 1) {
                zapiszPozycje(pozycja+1, appWidgetIdString, context);
            } else {
                zapiszPozycje(0, appWidgetIdString, context);
            }
        }

        String action = intent.getAction();

        if (action != null && action.equals("android.appwidget.action.APPWIDGET_DELETED")) {
            czyscPozycje(appWidgetIdString, context);
        }
    }

    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.notatki_widget);
        Intent intent = new Intent(context, NotatkiWidget.class);
        intent.putExtra("idString", Integer.toString(appWidgetId));
        intent.putExtra("id", appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setImageViewResource(R.id.imgWvIkona, R.drawable.ic_launcher_round);
        views.setOnClickPendingIntent(R.id.imgWvIkona, pendingIntent);
        String appWidgetIdString = Integer.toString(appWidgetId);
        widgetRoutine(views, context, appWidgetIdString, appWidgetManager, intent, appWidgetId);
    }

    @Override
    public void onReceive(Context context, Intent intent)  {
        super.onReceive(context, intent);
        String appWidgetIdString = intent.getStringExtra("idString");
        int appWidgetId = intent.getIntExtra("id", 0);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.notatki_widget);
        widgetRoutine(views, context, appWidgetIdString, AppWidgetManager.getInstance(context), intent, appWidgetId);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

