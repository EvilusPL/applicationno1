package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btnNotatki = findViewById(R.id.btnNotatki);
        final Button btnPrzypominajka = findViewById(R.id.btnPrzypominajka);
        final Button btnMultimedia = findViewById(R.id.btnMultimedia);
        final Button btnKsiazkaTelefoniczna = findViewById(R.id.btnKsiazkaTelefoniczna);
        final FloatingActionButton btnZamknij = findViewById(R.id.btnZamknij);


        btnNotatki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(MainActivity.this, WszystkieNotatki.class);
                startActivity(intent);
            }
        });

        btnZamknij.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnPrzypominajka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(MainActivity.this, Przypominajka.class);
                startActivity(intent);
            }
        });

        btnMultimedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OdtwarzaczMuzyki.class);
                startActivity(intent);
            }
        });

        btnKsiazkaTelefoniczna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, KsiazkaTelefoniczna.class);
                startActivity(intent);
            }
        });
    }
}
