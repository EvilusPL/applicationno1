package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class DodajKontakt extends AppCompatActivity {

    private ImageView imageView;
    private EditText imie;
    private EditText nazwisko;
    private TextInputLayout numerTelefonu;
    private EditText email;
    private Spinner wybierzPlec;
    private Button dodaj;
    private ArrayList<Kontakt> kontakty;
    private Kontakt kontakt;
    private int plec;
    private Uri imageUri;
    private boolean imageSelected;

    private ArrayList<Kontakt> odczytajKontakty(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("kontakty", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<Kontakt>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<Kontakt>();
    }

    private void zapiszKontakty(ArrayList<Kontakt> notatki, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("kontakty", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notatki);
        editor.putString(key, json);
        editor.apply();
    }

    public void addListenerOnSpinnerItemSelection() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sex, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wybierzPlec.setAdapter(adapter);

        wybierzPlec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Mężczyzna")) {
                    plec = 1;
                    imageView.setImageResource(R.drawable.man);
                } else {
                    plec = 0;
                    imageView.setImageResource(R.drawable.girl);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            imageUri = uri;
            imageView.setImageURI(imageUri);
            imageSelected = true;

        }
    }

    byte[] getBitmapAsByteArray(Bitmap bitmap, int quality, Bitmap.CompressFormat format) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(format, quality, outputStream);
        return outputStream.toByteArray();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kontakt);

        kontakty = odczytajKontakty("kontakty");

        imageView = findViewById(R.id.imageView);
        imie = findViewById(R.id.nameInput);
        nazwisko = findViewById(R.id.surnameInput);
        numerTelefonu = findViewById(R.id.numerInput);
        email = findViewById(R.id.emailInput);
        wybierzPlec = findViewById(R.id.spinner);
        dodaj = findViewById(R.id.button5);

        addListenerOnSpinnerItemSelection();

        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String plecString;
                Bitmap bitmap = null;
                byte[] awatar;
                if (plec == 1)
                    plecString = "Mężczyzna";
                else
                    plecString = "Kobieta";

                if (imageSelected == true) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    awatar = getBitmapAsByteArray(bitmap, 70, Bitmap.CompressFormat.JPEG);

                } else {
                    Drawable drawable;
                    Resources resources = getResources();
                    if (plec == 1) {
                        drawable = resources.getDrawable(R.drawable.man);
                    } else {
                        drawable = resources.getDrawable(R.drawable.girl);
                    }
                    bitmap = ((BitmapDrawable) drawable).getBitmap();

                    awatar = getBitmapAsByteArray(bitmap, 100, Bitmap.CompressFormat.PNG);
                }

                if (imie.getText().toString().length() == 0 || nazwisko.getText().toString().length() == 0 || numerTelefonu.getEditText().toString().length() == 0 || email.getText().toString().length() == 0) {
                    Toast.makeText(DodajKontakt.this, "Wypełnij wszystkie pola!", Toast.LENGTH_SHORT).show();
                } else {
                    kontakt = new Kontakt(imie.getText().toString(), nazwisko.getText().toString(), numerTelefonu.getEditText().getText().toString(), email.getText().toString(), plecString, awatar);
                    kontakty.add(kontakt);
                    zapiszKontakty(kontakty, "kontakty");
                    Toast.makeText(DodajKontakt.this, "Dodano nowy kontakt!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });
    }
}
