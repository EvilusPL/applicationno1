package ch.gotdns.kikuchi.applicationno1;

import java.util.Date;

public class FormatPrzypomnienia {
    private Date addedDate;
    private Date alarmDate;
    private String description;
    private String note;

    public String getAddedDate() {
        return addedDate.toString();
    }


    public Date getAlarmDate() {
        return alarmDate;
    }

    public String getDescription() {
        return description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setAddedDate() {
        addedDate = new Date();
    }

    public void setAlarmDate(Date alarmDate) {
        this.alarmDate = alarmDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    FormatPrzypomnienia(Date alarmDate, String description, String note) {
        addedDate = new Date();
        this.alarmDate = alarmDate;
        this.description = description;
        this.note = note;
    }
}
