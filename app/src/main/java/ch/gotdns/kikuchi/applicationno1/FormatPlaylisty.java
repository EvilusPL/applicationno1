package ch.gotdns.kikuchi.applicationno1;

public class FormatPlaylisty {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public FormatPlaylisty(long id, String title, String artist) {
        this.id = id;
        this.title = title;
        this.artist = artist;
    }

    private long id;
    private String title;
    private String artist;

}
