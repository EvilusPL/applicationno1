package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class WszystkieNotatki extends AppCompatActivity {

    private ArrayList<FormatNotatki> notatki;
    private ArrayList<FormatNotatki> allNotes;
    private ArrayList<Integer> notesPositions;

    private ListView lstwvListaNotatek;


    private ArrayAdapter<FormatNotatki> arrayAdapter;

    private ArrayList<FormatNotatki> odczytajNotatki(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("notatki", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatNotatki>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatNotatki>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wszystkie_notatki);

        FloatingActionButton btnDodaj = findViewById(R.id.btnDodaj);
        FloatingActionButton btnUsun = findViewById(R.id.btnUsun);
        Button btnSzukaj = findViewById(R.id.btnSzukaj);
        final EditText edTxtZapytanie = findViewById(R.id.edtxtZapytanie);
        lstwvListaNotatek = findViewById(R.id.lstwvListaNotatek);

        btnDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WszystkieNotatki.this, Notatka.class);
                startActivity(intent);
            }
        });

        btnSzukaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayAdapter.getFilter().filter(edTxtZapytanie.getText().toString());
            }
        });

        btnUsun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WszystkieNotatki.this, UsuwanieNotatki.class);
                startActivity(intent);
            }
        });

        lstwvListaNotatek.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PodgladNotatki.tytul = notatki.get(position).getTytul();
                PodgladNotatki.tresc = notatki.get(position).getTresc();
                PodgladNotatki.data = notatki.get(position).getDate();

                Intent intent = new Intent(WszystkieNotatki.this, PodgladNotatki.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        notatki = odczytajNotatki("notatki");
        allNotes = odczytajNotatki("notatki");

        arrayAdapter = new ArrayAdapter<FormatNotatki>(this, android.R.layout.simple_list_item_2, android.R.id.text1, notatki) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                TextView text1 = view.findViewById(android.R.id.text1);
                TextView text2 = view.findViewById(android.R.id.text2);
                text1.setText(notatki.get(position).getTytul());
                text2.setText(notatki.get(position).getDate());
                return view;
            }

            @Override
            public Filter getFilter() {
                Filter filter = new Filter() {
                    @Override
                    protected void publishResults(CharSequence constraint, FilterResults results) {
                        notatki = (ArrayList<FormatNotatki>) results.values;
                        if (notatki.size() == 0) {
                            Toast.makeText(WszystkieNotatki.this, "Nic nie znaleziono! :(", Toast.LENGTH_SHORT).show();
                        }
                        arrayAdapter.clear();
                        arrayAdapter.addAll(notatki);
                        notifyDataSetChanged();
                    }

                    @Override
                    protected FilterResults performFiltering(CharSequence constraint) {
                        FilterResults filterResults = new FilterResults();
                        ArrayList<FormatNotatki> resultsArray = new ArrayList<FormatNotatki>();

                        if (constraint == null || constraint.length() == 0) {
                            filterResults.values = allNotes;
                            filterResults.count = allNotes.size();
                        } else {
                            constraint = constraint.toString().toLowerCase();
                            for (int i=0; i<allNotes.size(); i++) {
                                String tytulNotatki = allNotes.get(i).getTytul();
                                if (tytulNotatki.toLowerCase().contains(constraint)) {
                                    resultsArray.add(allNotes.get(i));
                                }
                            }
                            filterResults.count = resultsArray.size();
                            filterResults.values = resultsArray;
                        }

                        return filterResults;
                    }
                };
                return filter;
            }
        };

        lstwvListaNotatek.setAdapter(arrayAdapter);
    }
}
