package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EdytujPrzypomnienie extends AppCompatActivity {

    private ArrayList<FormatPrzypomnienia> przypomnienia;
    private Calendar calendar;
    private EditText chooseTime;
    private EditText reminderName;
    private EditText note;

    private void zapiszPrzypomnienie(ArrayList<FormatPrzypomnienia> przypomnienia, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(przypomnienia);
        editor.putString(key, json);
        editor.apply();
    }

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edytuj_przypomnienie);

        final CalendarView calendarView = findViewById(R.id.inputDate2);
        chooseTime = findViewById(R.id.inputTime2);
        reminderName = findViewById(R.id.reminderName2);
        note = findViewById(R.id.inputNote2);
        FloatingActionButton addReminder = findViewById(R.id.addReminder2);
        FloatingActionButton deleteReminder = findViewById(R.id.deleteReminder);

        final SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm:ss");

        przypomnienia = odczytajPrzypomnienia("przypomnienia");

        calendar = Calendar.getInstance();

        Intent intent = getIntent();
        final int reminderId = intent.getIntExtra("reminderId", 0);

        reminderName.setText(przypomnienia.get(reminderId).getDescription());
        calendarView.setDate(przypomnienia.get(reminderId).getAlarmDate().getTime());
        Date time = przypomnienia.get(reminderId).getAlarmDate();
        chooseTime.setText(simpleDateFormatTime.format(time));
        note.setText(przypomnienia.get(reminderId).getNote());


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                calendar = new GregorianCalendar(year, month, dayOfMonth);
            }
        });

        addReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date time = simpleDateFormatTime.parse(chooseTime.getText().toString());
                    Calendar calendarTime = Calendar.getInstance();
                    calendarTime.setTime(time);
                    calendar.set(Calendar.HOUR, calendarTime.get(Calendar.HOUR_OF_DAY));
                    calendar.set(Calendar.MINUTE, calendarTime.get(Calendar.MINUTE));
                    calendar.set(Calendar.SECOND, calendarTime.get(Calendar.SECOND));
                    if (reminderName.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Przypomnienie musi zawierać tytuł!", Toast.LENGTH_SHORT).show();
                    } else if (calendar.getTime().compareTo(new Date()) < 0) {
                        Toast.makeText(getApplicationContext(), "Nie można stworzyć przypomnienia na dzień wcześniejszy!", Toast.LENGTH_SHORT).show();
                    } else {

                        Intent cancelOldAlarm = new Intent(EdytujPrzypomnienie.this, UslugaAlarmu.class);
                        cancelOldAlarm.putExtra("notificationId", reminderId);
                        cancelOldAlarm.setAction(UslugaAlarmu.CANCEL);
                        startService(cancelOldAlarm);

                        przypomnienia.get(reminderId).setAlarmDate(calendar.getTime());
                        przypomnienia.get(reminderId).setDescription(reminderName.getText().toString());
                        przypomnienia.get(reminderId).setAddedDate();
                        przypomnienia.get(reminderId).setNote(note.getText().toString());

                        Intent startNewAlarm = new Intent(EdytujPrzypomnienie.this, UslugaAlarmu.class);
                        startNewAlarm.putExtra("notificationId", reminderId);
                        startNewAlarm.setAction(UslugaAlarmu.CREATE);
                        startService(startNewAlarm);

                        zapiszPrzypomnienie(przypomnienia, "przypomnienia");

                        Toast.makeText(getApplicationContext(), "Zapisano nowe przypomnienie!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

       deleteReminder.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent removeAlarm = new Intent(EdytujPrzypomnienie.this, UslugaAlarmu.class);
               removeAlarm.putExtra("notificationId", reminderId);
               removeAlarm.setAction(UslugaAlarmu.REMOVE);
               startService(removeAlarm);
               Toast.makeText(EdytujPrzypomnienie.this, "Usunięto przypomnienie!", Toast.LENGTH_SHORT).show();
               finish();
           }
       });
    }
}
