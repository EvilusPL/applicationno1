package ch.gotdns.kikuchi.applicationno1;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;

public class FormatNotatki {
    private String tytul;
    private String tresc;
    private Date data;

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public void setDate(Date data) { this.data = data; }

    public String getTytul() {
        return tytul;
    }

    public String getTresc() {
        return tresc;
    }

    public String getDate() {
        return data.toString();
    }



    FormatNotatki(String tytul, String tresc) {
        this.tytul = tytul;
        this.tresc = tresc;
        this.data = new Date();
    }
}
