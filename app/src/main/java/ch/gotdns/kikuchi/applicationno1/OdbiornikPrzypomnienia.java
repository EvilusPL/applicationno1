package ch.gotdns.kikuchi.applicationno1;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class OdbiornikPrzypomnienia extends BroadcastReceiver {

    public static final String NOTIFICATION_CHANNEL_ID = "182019";

    @Override
    public void onReceive(Context context, Intent intent)  {
        Log.d(this.toString(), "ALARM!");
        int reminderId = intent.getIntExtra("reminderId", 0);
        String description = intent.getStringExtra("description");

        Intent singleReminderIntent = new Intent(context, PojedynczePrzypomnienie.class);
        singleReminderIntent.putExtra("reminderId", reminderId);
        singleReminderIntent.putExtra("key", "przypomnienia");

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, singleReminderIntent, 0);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(description)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_ALARM_ALERT_URI)
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_REMINDER", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert notificationChannel != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        assert notificationManager != null;
        notificationManager.notify(reminderId, builder.build());

    }

}
