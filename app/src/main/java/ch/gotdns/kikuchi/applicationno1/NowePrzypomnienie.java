package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class NowePrzypomnienie extends AppCompatActivity {

    private ArrayList<FormatPrzypomnienia> przypomnienia;
    private Calendar calendar;
    private EditText chooseTime;
    private EditText reminderName;
    private EditText note;

    private void zapiszPrzypomnienie(ArrayList<FormatPrzypomnienia> przypomnienia, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(przypomnienia);
        editor.putString(key, json);
        editor.apply();
    }

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(NowePrzypomnienie.this.getLocalClassName(), "created");
        setContentView(R.layout.activity_nowe_przypomnienie);

        final CalendarView calendarView = findViewById(R.id.inputDate);
        chooseTime = findViewById(R.id.inputTime);
        reminderName = findViewById(R.id.reminderName);
        note = findViewById(R.id.inputNote);
        FloatingActionButton addReminder = findViewById(R.id.addReminder);

        przypomnienia = odczytajPrzypomnienia("przypomnienia");

        calendar = Calendar.getInstance();

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                calendar = new GregorianCalendar(year, month, dayOfMonth);
            }
        });

        addReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm:ss");
                    Date time = simpleDateFormatTime.parse(chooseTime.getText().toString());
                    Calendar calendarTime = Calendar.getInstance();
                    calendarTime.setTime(time);
                    calendar.set(Calendar.HOUR, calendarTime.get(Calendar.HOUR_OF_DAY));
                    calendar.set(Calendar.MINUTE, calendarTime.get(Calendar.MINUTE));
                    calendar.set(Calendar.SECOND, calendarTime.get(Calendar.SECOND));
                    if (reminderName.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Przypomnienie musi zawierać tytuł!", Toast.LENGTH_SHORT).show();
                    } else if (calendar.getTime().compareTo(new Date()) < 0) {
                        Toast.makeText(getApplicationContext(), "Nie można stworzyć przypomnienia na dzień wcześniejszy!", Toast.LENGTH_SHORT).show();
                    } else {
                        FormatPrzypomnienia przypomnienie = new FormatPrzypomnienia(calendar.getTime(), reminderName.getText().toString(), note.getText().toString());
                        przypomnienia.add(przypomnienie);

                        Intent startNewAlarm = new Intent(NowePrzypomnienie.this, UslugaAlarmu.class);
                        startNewAlarm.putExtra("notificationId", przypomnienia.indexOf(przypomnienie));
                        startNewAlarm.setAction(UslugaAlarmu.CREATE);
                        startService(startNewAlarm);

                        zapiszPrzypomnienie(przypomnienia, "przypomnienia");
                        Toast.makeText(getApplicationContext(), "Zapisano nowe przypomnienie!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
