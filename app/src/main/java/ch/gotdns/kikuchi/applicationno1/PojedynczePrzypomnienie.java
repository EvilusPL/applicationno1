package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class PojedynczePrzypomnienie extends AppCompatActivity {

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    private void zapiszPrzypomnienie(ArrayList<FormatPrzypomnienia> przypomnienia, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(przypomnienia);
        editor.putString(key, json);
        editor.apply();
    }

    private void przeniesDoArchiwum() {
        ArrayList<FormatPrzypomnienia> doPrzeniesienia = odczytajPrzypomnienia("przypomnienia");
        ArrayList<FormatPrzypomnienia> archiwum = odczytajPrzypomnienia("archiwum");
        for (int i=0; i<doPrzeniesienia.size(); i++) {
            if (doPrzeniesienia.get(i).getAlarmDate().compareTo(new Date()) < 0) {
                archiwum.add(doPrzeniesienia.get(i));
            }
        }
        for (int i=0; i<archiwum.size(); i++) {
            doPrzeniesienia.remove(archiwum.get(i));
        }
        zapiszPrzypomnienie(doPrzeniesienia, "przypomnienia");
        zapiszPrzypomnienie(archiwum, "archiwum");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pojedyncze_przypomnienie);

        Intent intent = getIntent();
        int reminderId = intent.getIntExtra("reminderId", 0);

        ArrayList<FormatPrzypomnienia> przypomnienia = odczytajPrzypomnienia("przypomnienia");

        TextView txtDescription = findViewById(R.id.txtDescription);
        TextView txtAlarmDate = findViewById(R.id.txtAlarmDatte);
        TextView txtCreatedDate = findViewById(R.id.txtCreatedDate);
        EditText notePreview = findViewById(R.id.notePreview);
        Button btnReturn = findViewById(R.id.btnReturn);

        notePreview.setEnabled(false);


        txtDescription.setText(przypomnienia.get(reminderId).getDescription());
        txtAlarmDate.setText("Wywołano: "+przypomnienia.get(reminderId).getAlarmDate());
        txtCreatedDate.setText("Utworzono: "+przypomnienia.get(reminderId).getAddedDate());
        notePreview.setText(przypomnienia.get(reminderId).getNote());

        przeniesDoArchiwum();

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
