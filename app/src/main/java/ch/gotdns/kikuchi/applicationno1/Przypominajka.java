package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class Przypominajka extends AppCompatActivity {

    private ArrayList<FormatPrzypomnienia> przypomnienia;
    private ArrayAdapter<FormatPrzypomnienia> arrayAdapter;

    private ListView lstPrzypomnienia;

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_przypominajka);
        FloatingActionButton btnDodajPrzypomnienie = findViewById(R.id.btnDodajPrzypomnienie);
        FloatingActionButton btnArchiwum = findViewById(R.id.btnArchiwum);
        lstPrzypomnienia = findViewById(R.id.lstPrzypomnienia);


        btnDodajPrzypomnienie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Przypominajka.this, NowePrzypomnienie.class);
                startActivity(intent);
            }
        });

        btnArchiwum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Przypominajka.this, ArchiwumPrzypomnien.class);
                startActivity(intent);
            }
        });

        lstPrzypomnienia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Przypominajka.this, EdytujPrzypomnienie.class);
                intent.putExtra("reminderId", position);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        przypomnienia = odczytajPrzypomnienia("przypomnienia");
        arrayAdapter = new ArrayAdapter<FormatPrzypomnienia>(this, android.R.layout.simple_list_item_2, android.R.id.text1, przypomnienia) {
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final View view = super.getView(position, convertView, parent);
                TextView text1 = view.findViewById(android.R.id.text1);
                TextView text2 = view.findViewById(android.R.id.text2);
                text1.setText(przypomnienia.get(position).getDescription());
                text2.setText(przypomnienia.get(position).getAlarmDate().toString());
                return view;
            }
        };
        lstPrzypomnienia.setAdapter(arrayAdapter);
    }

}
