package ch.gotdns.kikuchi.applicationno1;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Notatka extends AppCompatActivity {

    ArrayList<FormatNotatki> notatki;

    private void zapiszNotatke(ArrayList<FormatNotatki> notatki, String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("notatki", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(notatki);
        editor.putString(key, json);
        editor.apply();
    }

    private ArrayList<FormatNotatki> odczytajNotatki(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("notatki", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatNotatki>>() {}.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatNotatki>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notatka);

        final SharedPreferences sharedPreferences = getSharedPreferences("notatki", MODE_PRIVATE);
        Button btnZapisz = findViewById(R.id.btnZapisz);
        final EditText edtxtTytulNotatki = findViewById(R.id.edtxtTytulNotatki);
        final EditText edtxtTrescNotatki = findViewById(R.id.edtxtTrescNotatki);

        notatki = odczytajNotatki("notatki");

        btnZapisz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtxtTrescNotatki.length() == 0) {
                    Toast.makeText(Notatka.this, "Notatka musi zawierać tytuł!", Toast.LENGTH_SHORT).show();
                } else {
                    notatki.add(new FormatNotatki(edtxtTytulNotatki.getText().toString(), edtxtTrescNotatki.getText().toString()));
                    zapiszNotatke(notatki, "notatki");
                    Toast.makeText(Notatka.this, "Zapisano nowa notatke!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
