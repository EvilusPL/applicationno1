package ch.gotdns.kikuchi.applicationno1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PojedynczePrzypomnienieArchiwum extends AppCompatActivity {

    private ArrayList<FormatPrzypomnienia> odczytajPrzypomnienia(String key) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("przypomnienia", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<FormatPrzypomnienia>>() {
        }.getType();
        if (gson.fromJson(json, type) != null) {
            return gson.fromJson(json, type);
        }
        return new ArrayList<FormatPrzypomnienia>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pojedyncze_przypomnienie_archiwum);

        Intent intent = getIntent();
        int reminderId = intent.getIntExtra("reminderId", 0);

        ArrayList<FormatPrzypomnienia> przypomnienia = odczytajPrzypomnienia("archiwum");

        TextView txtDescription = findViewById(R.id.txtDescription2);
        TextView txtAlarmDate = findViewById(R.id.txtAlarmDatte2);
        TextView txtCreatedDate = findViewById(R.id.txtCreatedDate2);
        EditText notePreview = findViewById(R.id.notePreview2);
        Button btnReturn = findViewById(R.id.btnReturn2);

        notePreview.setEnabled(false);


        txtDescription.setText(przypomnienia.get(reminderId).getDescription());
        txtAlarmDate.setText("Wywołano: " + przypomnienia.get(reminderId).getAlarmDate());
        txtCreatedDate.setText("Utworzono: " + przypomnienia.get(reminderId).getAddedDate());
        notePreview.setText(przypomnienia.get(reminderId).getNote());

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
