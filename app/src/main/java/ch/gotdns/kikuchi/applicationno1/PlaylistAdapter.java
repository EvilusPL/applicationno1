package ch.gotdns.kikuchi.applicationno1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PlaylistAdapter extends BaseAdapter {

    private ArrayList<FormatPlaylisty> playlist;
    private LayoutInflater layoutInflater;

    public PlaylistAdapter(Context context, ArrayList<FormatPlaylisty> playlist) {
        this.playlist = playlist;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return playlist.size();
    }

    @Override
    public Object getItem(int position) {
        return playlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return playlist.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.row_song, parent, false);
        TextView songView = linearLayout.findViewById(R.id.song_title);
        TextView artistView = linearLayout.findViewById(R.id.song_artist);
        FormatPlaylisty currentSong = playlist.get(position);

        songView.setText(currentSong.getTitle());
        artistView.setText(currentSong.getArtist());
        linearLayout.setTag(position);

        return linearLayout;
    }
}
